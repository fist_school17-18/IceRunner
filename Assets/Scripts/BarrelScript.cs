﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BarrelScript : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Animator anim;
        if (anim = collision.GetComponent<Animator>())
        {
            anim.SetBool("melt", true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Animator anim;
        if (anim = collision.GetComponent<Animator>())
        {
            anim.SetBool("melt", false);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (collision.transform.localScale.x >= 0.15)
                collision.transform.localScale -=
                    new Vector3(1, 1, 0) * 0.5f * Time.deltaTime;
            else
                Destroy(collision.gameObject);

        }
    }
}
