﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnedObject : MonoBehaviour {
    public int speed = 10;
	void Update () {
        transform.position += Vector3.left * Time.deltaTime * speed;	
	}
}
