﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformPooler : MonoBehaviour {
#region singleton
    private static PlatformPooler _instance;
    public static PlatformPooler Instance()
    {
        return _instance;
    }
    private void Awake()
    {
        if (_instance == null)
            _instance = this;
    }
    #endregion singleton
    [HideInInspector] public bool gameOver = false;
    public int poolSize;

    //Dictionary<string, GameObject> dic;
    //public GameObject[] prefabs;
    public GameObject prefab;
    //List<List<GameObject>> pools = new List<List<GameObject>>();
    List<GameObject> pool = new List<GameObject>();

    //Queue<GameObject> myQueue = new Queue<GameObject>();

    private void Start()
    {
        //foreach (GameObject prefab in prefabs)
        //{
            //List<GameObject> pool = new List<GameObject>();
            for (int i = 0; i < poolSize; i++)
            {
                GameObject go = Instantiate(prefab);
                go.SetActive(false);
                pool.Add(go);
            }
            //pools.Add(pool);
        //}
    }

    public GameObject PopFromPool()
    {
        GameObject go = pool[0];
        go.SetActive(true);
        pool.RemoveAt(0);

        return go;
    }

    public void PushToPool(GameObject go)
    {
        go.SetActive(false);
        pool.Add(go);
    }
}
