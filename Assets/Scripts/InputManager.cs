﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour {
    Rigidbody2D rb;

	void Start ()
    {
        rb = GetComponent<Rigidbody2D>();
	}

	void Update ()
    {
	    if (Input.GetKeyDown(KeyCode.Space))
        {
            rb.velocity += Vector2.up * 10;
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            rb.velocity += Vector2.left * 10;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            rb.velocity += Vector2.right * 10;
        }


    }
}
