﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Destroyer : MonoBehaviour {
    public Text scoreText;
    int score = 0;

    List<GameObject> pool;
    void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.layer == 8)
        {
            collision.transform.position +=
                3 * 12 * Vector3.right;
            return;
        }

        if (collision.gameObject.tag == "Player")
        {
            PlatformPooler.Instance().gameOver = true;
            print("GameOver");
            Destroy(collision.gameObject);
        }

        score++;
        scoreText.text = score.ToString();

        PlatformPooler
            .Instance()
            .PushToPool(collision.gameObject);
    }
}
