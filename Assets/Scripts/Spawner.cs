﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

    public GameObject[] prefabs;
    public float minTime, maxTime;
	void Start ()
    {
        Invoke("Spawn", Random.Range(minTime, maxTime));
	}
    void Spawn ()
    {
        if (PlatformPooler.Instance().gameOver == true)
            return;
        /*
        Instantiate(
            prefabs[Random.Range(0, prefabs.Length)],
            transform.position + Vector3.up * Random.Range(-2, 2),
            Quaternion.identity
        );
        */
        GameObject go = PlatformPooler
            .Instance()
            .PopFromPool();
        go.transform.position =
             transform.position + Vector3.up * Random.Range(-2, 2);
        go.transform.rotation = 
            Quaternion.identity;


        Invoke("Spawn", Random.Range(minTime, maxTime));
    }
}
